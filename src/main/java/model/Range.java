package model;

public class Range {

  private int min;
  private int max;
  private int[] arr;

  public Range(final int num1, final int num2) {
    //Validating min and max
    if (num1 < num2) {
      this.min = num1;
      this.max = num2;
    } else {
      this.min = num2;
      this.max = num1;
    }
    createArray();
  }

  private void createArray() {
    this.arr = new int[(max - min) + 1];
    for (int i = min, j = 0; i <= max; i++, j++) {
      this.arr[j] = i;
    }
  }

  public final int[] getArr() {
    return arr;
  }
}
