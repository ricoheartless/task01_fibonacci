package model;

import exceptions.DummyException;
import utility.ArrayHelper;

public class FibonacciSequence implements AutoCloseable {

  private static final double PERCENTAGE = 100.0;
  private int num1;
  private int num2;
  private int sizeOfSet;
  private int[] set;

  public FibonacciSequence(final int arg1, final int arg2, final int sizeArg) {
    this.num1 = arg1;
    this.num2 = arg2;
    this.sizeOfSet = sizeArg;
    this.set = makeSet();
  }

  private int[] makeSet() {
    int[] arr = new int[sizeOfSet];
    arr[0] = num1;
    arr[1] = num2;
    for (int i = 2; i < sizeOfSet; ++i) {
      int next = num1 + num2;
      num1 = num2;
      num2 = next;
      arr[i] = next;
    }
    return arr;
  }

  public final int[] getSet() {
    return set;
  }

  public final double getPercentageOfOdd() {
    double oddPercentage = (double) (ArrayHelper.getOddArray(this.set).length)
        / (this.set.length);
    return oddPercentage * PERCENTAGE;
  }

  public final double getPercentageOfEven() {
    double evenPercentage = (double) (ArrayHelper.getEvenArray(this.set).length)
        / (this.set.length);
    return evenPercentage * PERCENTAGE;
  }

  @Override
  public final void close() throws Exception {
    throw new DummyException();
  }
}
