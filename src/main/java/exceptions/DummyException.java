package exceptions;

public class DummyException extends Exception {

  public final void showMessage() {
    System.out.println("You've got Dummy Exception!");
  }
}
