package exceptions;

public class WrongRangeParametersException extends Exception {

  public final void showMessage() {
    System.out.println("Your range parameters are invalid");
  }
}
