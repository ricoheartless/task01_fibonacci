package exceptions;

public class ParameterTooLowException extends Exception {

  public final void showMessage() {
    System.out.println("Your parameter is too low!");
  }
}
