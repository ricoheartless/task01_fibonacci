package utility;

import java.util.ArrayList;

public final class ArrayHelper {

  private ArrayHelper() {
  }

  private static ArrayList<Integer> arrayList;

  public static int[] getOddArray(final int[] arr) {
    arrayList = new ArrayList();
    for (int i = 0; i < arr.length; i++) {
      if (arr[i] % 2 != 0) {
        arrayList.add(arr[i]);
      }
    }
    return arrayList.stream().mapToInt(i -> i).toArray();
  }

  public static int[] getEvenArray(final int[] arr) {
    arrayList = new ArrayList();
    for (int i = 0; i < arr.length; i++) {
      if (arr[i] % 2 == 0) {
        arrayList.add(arr[i]);
      }
    }
    return arrayList.stream().mapToInt(i -> i).toArray();
  }

  public static int getMax(final int[] arr) {
    int num = arr[0];
    for (int i = 1; i < arr.length; i++) {
      if (num < arr[i]) {
        num = arr[i];
      }
    }
    return num;
  }

  public static int getMin(final int[] arr) {
    int num = arr[0];
    for (int i = 1; i < arr.length; i++) {
      if (num > arr[i]) {
        num = arr[i];
      }
    }
    return num;
  }

  public static int getSum(final int[] arr) {
    int sum = 0;
    for (int i = 0; i < arr.length; i++) {
      sum += arr[i];
    }
    return sum;
  }
}
