package utility;

import exceptions.ParameterTooLowException;
import exceptions.WrongRangeParametersException;
import java.util.Scanner;
import model.Range;

public final class ConsoleReader {

  private ConsoleReader() {
  }

  private static Scanner scanner;
  private static final int MIN_NUM = 0;

  public static int readInt(final String message) {
    scanner = new Scanner(System.in);
    System.out.println(message);
    while (!scanner.hasNextInt()) {
      scanner.next();
    }
    return scanner.nextInt();
  }

  public static int readFibonacci(final String message)
      throws ParameterTooLowException {
    boolean valid = false;
    int num = 0;
    while (!valid) {
      num = readInt(message);
      if (num < MIN_NUM) {
        throw new ParameterTooLowException();
      } else {
        valid = true;
      }
    }
    return num;
  }

  public static Range readRange(final String message)
      throws WrongRangeParametersException {
    int temp = 0;
    scanner = new Scanner(System.in);
    System.out.println(message);
    while (!scanner.hasNextInt()) {
      scanner.next();
    }
    int num1 = scanner.nextInt();
    while (scanner.hasNextInt()) {
      temp = scanner.nextInt();
      if (num1 == temp) {
        throw new WrongRangeParametersException();
      } else {
        break;
      }
    }
    int num2 = temp;
    return new Range(num1, num2);
  }

  public static char readChar(final String msg) {
    scanner = new Scanner(System.in);
    System.out.println(msg);
    char input = scanner.nextLine().charAt(0);
    return input;
  }
}
