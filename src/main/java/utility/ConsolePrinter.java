package utility;

public final class ConsolePrinter {

  private ConsolePrinter() {
  }

  public static void printFromStart(final int[] arr) {
    for (int i = 0; i < arr.length; i++) {
      System.out.print(arr[i] + " ");
    }
    System.out.println();
  }

  public static void printFromEnd(final int[] arr) {
    for (int i = arr.length - 1; i >= 0; i--) {
      System.out.print(arr[i] + " ");
    }
    System.out.println();
  }
}
