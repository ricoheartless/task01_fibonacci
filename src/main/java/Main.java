import exceptions.DummyException;
import exceptions.ParameterTooLowException;
import exceptions.WrongRangeParametersException;
import model.FibonacciSequence;
import model.Range;
import utility.ArrayHelper;
import utility.ConsolePrinter;
import utility.ConsoleReader;

public final class Main {

  private Main() {
  }

  public static void main(final String[] args) {
    while (true) {
      Range range;
      try {
        range = ConsoleReader.readRange("hello, enter nums: ");
      } catch (WrongRangeParametersException e) {
        e.showMessage();
        continue;
      }
      int[] odds = ArrayHelper.getOddArray(range.getArr());
      int[] evens = ArrayHelper.getEvenArray(range.getArr());
      ConsolePrinter.printFromStart(odds);
      ConsolePrinter.printFromEnd(evens);
      System.out.println("Sum of odd numbers: "
          + ArrayHelper.getSum(odds));
      System.out.println("Sum of even numbers: "
          + ArrayHelper.getSum(evens));
      int f1 = ArrayHelper.getMax(odds);
      int f2 = ArrayHelper.getMax(evens);
      int fiboNum = 0;
      try {
        fiboNum = ConsoleReader.readFibonacci("Enter num of Fibonacci set: ");
      } catch (ParameterTooLowException e) {
        System.out.print("Something went wrong! Exception msg: ");
        e.showMessage();
      }
      try (FibonacciSequence set = new FibonacciSequence(f1, f2, fiboNum)) {
        ConsolePrinter.printFromStart(set.getSet());
        System.out.println("Percentage of odd: "
            + set.getPercentageOfOdd()
            + "%");
        System.out.println("Percentage of even: "
            + set.getPercentageOfEven()
            + "%");
      } catch (DummyException e) {
        System.out.print("Something went wrong with your fibonacci sequence: ");
        e.showMessage();
      } catch (Exception e) {
        System.out.println("Handle of Exception");
      }
      char input = ConsoleReader.readChar("Want to start again? 'y'/'n'");
      while (input != 'y' && input != 'n') {
        input = ConsoleReader.readChar("Enter correct answer! ('y'/'n')");
      }
      if (input != 'n') {
        continue;
      } else {
        System.exit(0);
      }
    }
  }
}
